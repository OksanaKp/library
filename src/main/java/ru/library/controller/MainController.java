package ru.library.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import ru.library.models.Students;
import ru.library.service.IssuesService;

@Controller
public class MainController {
    @Autowired
    IssuesService issuesService;

    @GetMapping("/index")
    public String addGet(Model model){
        Students worstReader = issuesService.findWorstReader(14);
        model.addAttribute("student", worstReader);
        return "/index";
    }
}
