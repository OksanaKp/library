package ru.library.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.library.models.Issues;

public interface IssuesRepo extends JpaRepository<Issues, Long> {
}
