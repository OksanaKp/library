package ru.library.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.library.models.Authors;

public interface AuthorsRepo extends JpaRepository<Authors, Long> {
}
