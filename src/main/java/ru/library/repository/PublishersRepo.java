package ru.library.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.library.models.Publishers;

public interface PublishersRepo extends JpaRepository<Publishers, Long> {
}
