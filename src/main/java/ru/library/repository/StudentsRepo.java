package ru.library.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.library.models.Students;

public interface StudentsRepo extends JpaRepository<Students, Long> {
}
