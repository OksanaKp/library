package ru.library.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.library.models.BookCopies;

public interface BookCopiesRepo extends JpaRepository<BookCopies, Long> {
}
