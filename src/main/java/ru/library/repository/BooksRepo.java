package ru.library.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.library.models.Books;

public interface BooksRepo extends JpaRepository<Books, Long> {
}
