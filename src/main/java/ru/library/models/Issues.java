package ru.library.models;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "issues")
public class Issues {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "issue_date")
    private Date issueDate;

    @Column(name = "return_date")
    private Date returnDate;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "bookCopies_id")
    private BookCopies bookCopies;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "students_id")
    private Students students;


    public Issues() {
    }

    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    public Date getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    public Date getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }

    public BookCopies getBookCopies() {
        return bookCopies;
    }

    public void setBookCopies(BookCopies bookCopies) {
        this.bookCopies = bookCopies;
    }

    public Students getStudents() {
        return students;
    }

    public void setStudents(Students students) {
        this.students = students;
    }
}
