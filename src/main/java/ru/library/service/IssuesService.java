package ru.library.service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.library.models.Issues;
import ru.library.models.Students;
import ru.library.repository.IssuesRepo;

import java.util.*;

@Service
public class IssuesService {

    @Autowired
    IssuesRepo issuesRepo;
    @Autowired
    StudentsService studentsService;

    public Students findWorstReader(int issuingPeriodCountDays) {
        return findWorstReader(issuesRepo.findAll(), issuingPeriodCountDays);
    }

    //функция для нахождения самого злостного читателя
    Students findWorstReader(List<Issues> issues, int issuingPeriodCountDays){
        final int day2msKoeff = 24 * 60 * 60 * 1000;
        Map<Long,Integer> studends = new HashMap<>();
        for (int i = 0; i< issues.size();i++){
            Long continueStudentId = issues.get(i).getStudents().getId();
            Long readingPeriod;
            if (issues.get(i).getReturnDate() != null) {
                //читатель уже вернул книгу
                readingPeriod = issues.get(i).getReturnDate().getTime() - issues.get(i).getIssueDate().getTime();
            }
            else {
                //читатель еще не вернул книгу
                readingPeriod = issues.get(i).getReturnDate().getTime() - (new Date()).getTime();
            }
            //если период нахождения книги у читателя больше, чем установлено библиотекой
            if (readingPeriod > issuingPeriodCountDays * day2msKoeff) {
                if (!studends.containsKey(continueStudentId)) {
                    studends.put(continueStudentId, 1);
                } else {
                    studends.put(continueStudentId, studends.get(continueStudentId) + 1);
                }
            }
        }
        Students worstReader = studentsService.getById(Collections.max(studends.entrySet(), Map.Entry.comparingByValue()).getKey());
        return worstReader;
    }
}
