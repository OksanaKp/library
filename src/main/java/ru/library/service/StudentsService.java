package ru.library.service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.library.models.Students;
import ru.library.repository.StudentsRepo;

@Service
public class StudentsService {

    @Autowired
    StudentsRepo studentsRepo;

    public Students getById(Long id) {
        return studentsRepo.getOne(id);
    }
}
