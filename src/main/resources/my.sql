﻿create database library;

INSERT INTO authors ("id", "first_name", "last_name", "patronymic") VALUES (1, 'Александр', 'Пушкин', 'Сергеевич');
INSERT INTO authors ("id", "first_name", "last_name", "patronymic") VALUES (2, 'Роберт', 'Стивенсон', 'Луис');
INSERT INTO authors ("id", "first_name", "last_name", "patronymic") VALUES (3, 'Жюль', 'Верн', 'Габриель');


INSERT INTO publishers ("id", "title") VALUES (1, 'Эксмо');
INSERT INTO publishers ("id", "title") VALUES (2, 'Вече');
INSERT INTO publishers ("id", "title") VALUES (3, 'АСТ');


INSERT INTO books ("id", "publishing_year", "title", "publisher_id") VALUES (1, 2015, 'Евгений Онегин', 1);
INSERT INTO books ("id", "publishing_year", "title", "publisher_id") VALUES (2, 2020, 'Черная стрела. Остров сокровищ', 2);
INSERT INTO books ("id", "publishing_year", "title", "publisher_id") VALUES (3, 2020, 'Ночлег Франсуа Вийона', 3);
INSERT INTO books ("id", "publishing_year", "title", "publisher_id") VALUES (4, 2019, 'Секрет Пандоры. Принц Отто', 2);
INSERT INTO books ("id", "publishing_year", "title", "publisher_id") VALUES (5, 2019, 'Путешествие к центру Земли', 1);
INSERT INTO books ("id", "publishing_year", "title", "publisher_id") VALUES (6, 2018, 'Двадцать тысяч лье под водой', 3);
INSERT INTO books ("id", "publishing_year", "title", "publisher_id") VALUES (7, 2018, 'Пятнадцатилетний капитан', 2);


INSERT INTO author_book ("book_id", "author_id") VALUES (1, 1);
INSERT INTO author_book ("book_id", "author_id") VALUES (2, 2);
INSERT INTO author_book ("book_id", "author_id") VALUES (3, 2);
INSERT INTO author_book ("book_id", "author_id") VALUES (4, 2);
INSERT INTO author_book ("book_id", "author_id") VALUES (5, 3);
INSERT INTO author_book ("book_id", "author_id") VALUES (6, 3);
INSERT INTO author_book ("book_id", "author_id") VALUES (7, 3);


INSERT INTO book_copies ("id", "inventory_number", "book_id") VALUES (1, 111111, 1);
INSERT INTO book_copies ("id", "inventory_number", "book_id") VALUES (2, 111112, 1);
INSERT INTO book_copies ("id", "inventory_number", "book_id") VALUES (3, 111113, 2);
INSERT INTO book_copies ("id", "inventory_number", "book_id") VALUES (4, 111114, 2);
INSERT INTO book_copies ("id", "inventory_number", "book_id") VALUES (5, 111115, 3);
INSERT INTO book_copies ("id", "inventory_number", "book_id") VALUES (6, 111116, 3);
INSERT INTO book_copies ("id", "inventory_number", "book_id") VALUES (7, 111117, 4);
INSERT INTO book_copies ("id", "inventory_number", "book_id") VALUES (8, 111118, 5);
INSERT INTO book_copies ("id", "inventory_number", "book_id") VALUES (9, 111119, 6);
INSERT INTO book_copies ("id", "inventory_number", "book_id") VALUES (10, 111100, 7);


INSERT INTO students ("id", "first_name", "last_name", "patronymic") VALUES (1, 'Иван', 'Иванов', 'Иванович');
INSERT INTO students ("id", "first_name", "last_name", "patronymic") VALUES (2, 'Ольга', 'Петрова', 'Игоревна');
INSERT INTO students ("id", "first_name", "last_name", "patronymic") VALUES (3, 'Максим', 'Ильиных', 'Николаевич');
INSERT INTO students ("id", "first_name", "last_name", "patronymic") VALUES (4, 'Федор', 'Фин', 'Степанович');
INSERT INTO students ("id", "first_name", "last_name", "patronymic") VALUES (5, 'Инга', 'Макарова', 'Олеговна');
INSERT INTO students ("id", "first_name", "last_name", "patronymic") VALUES (6, 'Вадим', 'Зимин', 'Михайлович');
INSERT INTO students ("id", "first_name", "last_name", "patronymic") VALUES (7, 'Елена', 'Крылова', 'Васильевна');
INSERT INTO students ("id", "first_name", "last_name", "patronymic") VALUES (8, 'Никита', 'Денисенко', 'Игоревич');
INSERT INTO students ("id", "first_name", "last_name", "patronymic") VALUES (9, 'Лариса', 'Денисенко', 'Игоревна');
INSERT INTO students ("id", "first_name", "last_name", "patronymic") VALUES (10, 'Иван', 'Столбов', 'Егорович');


INSERT INTO issues ("id", "issue_date", "return_date", "book_copies_id", "students_id") VALUES (1, '2020-03-01', '2020-03-10', 1, 4);
INSERT INTO issues ("id", "issue_date", "return_date", "book_copies_id", "students_id") VALUES (2, '2020-03-01', '2020-08-01', 2, 1);
INSERT INTO issues ("id", "issue_date", "return_date", "book_copies_id", "students_id") VALUES (3, '2020-03-01', '2020-03-13', 3, 5);
INSERT INTO issues ("id", "issue_date", "return_date", "book_copies_id", "students_id") VALUES (4, '2020-06-01', '2020-12-01', 4, 2);
INSERT INTO issues ("id", "issue_date", "return_date", "book_copies_id", "students_id") VALUES (5, '2020-06-01', '2020-12-01', 5, 2);
INSERT INTO issues ("id", "issue_date", "return_date", "book_copies_id", "students_id") VALUES (6, '2020-07-01', '2020-07-10', 6, 6);
INSERT INTO issues ("id", "issue_date", "return_date", "book_copies_id", "students_id") VALUES (7, '2020-07-01', '2020-12-01', 7, 3);
INSERT INTO issues ("id", "issue_date", "return_date", "book_copies_id", "students_id") VALUES (8, '2020-08-01', '2020-12-01', 8, 3);
INSERT INTO issues ("id", "issue_date", "return_date", "book_copies_id", "students_id") VALUES (9, '2020-08-01', '2020-12-01', 9, 3);
INSERT INTO issues ("id", "issue_date", "return_date", "book_copies_id", "students_id") VALUES (10, '2020-08-01', '2020-08-12', 10, 7);
INSERT INTO issues ("id", "issue_date", "return_date", "book_copies_id", "students_id") VALUES (11, '2020-09-01', '2020-09-11', 1, 8);
INSERT INTO issues ("id", "issue_date", "return_date", "book_copies_id", "students_id") VALUES (12, '2020-09-01', '2020-09-07', 2, 9);
INSERT INTO issues ("id", "issue_date", "return_date", "book_copies_id", "students_id") VALUES (13, '2020-09-01', '2020-09-08', 3, 10);
INSERT INTO issues ("id", "issue_date", "return_date", "book_copies_id", "students_id") VALUES (14, '2020-09-02', '2020-09-04', 4, 10);
INSERT INTO issues ("id", "issue_date", "return_date", "book_copies_id", "students_id") VALUES (15, '2020-09-02', '2020-12-01', 5, 3);

INSERT INTO issues ("id", "issue_date", "return_date", "book_copies_id", "students_id") VALUES (1, '2019-12-01', '2019-12-11', 8, 1);
INSERT INTO issues ("id", "issue_date", "return_date", "book_copies_id", "students_id") VALUES (2, '2019-12-01', '2019-12-10', 9, 2);
INSERT INTO issues ("id", "issue_date", "return_date", "book_copies_id", "students_id") VALUES (3, '2019-12-01', '2020-12-14', 10, 3);
INSERT INTO issues ("id", "issue_date", "return_date", "book_copies_id", "students_id") VALUES (4, '2018-12-01', '2018-12-10', 8, 4);
INSERT INTO issues ("id", "issue_date", "return_date", "book_copies_id", "students_id") VALUES (5, '2018-12-01', '2018-12-08', 9, 5);
INSERT INTO issues ("id", "issue_date", "return_date", "book_copies_id", "students_id") VALUES (6, '2018-12-01', '2018-12-20', 10, 6);

-- поиск самого популярного автора за все время
-- select a.id, a.last_name, a.first_name, a.patronymic, count(*)
-- from issues
--     join book_copies bc on issues.book_copies_id = bc.id
--     join books b on bc.book_id = b.id
--     join author_book ab on b.id = ab.book_id
--     join authors a on ab.author_id = a.id
--     group by a.id
--     order by count(*) desc
--     LIMIT 1;

-- поиск самого популярного автора за год
select a.id, a.last_name, a.first_name, a.patronymic, count(*)
from issues i
    join book_copies bc on i.book_copies_id = bc.id
    join books b on bc.book_id = b.id
    join author_book ab on b.id = ab.book_id
    join authors a on ab.author_id = a.id
where i.issue_date  > current_date - interval '1 year'
    group by a.id
    order by count(*) desc
    LIMIT 1;

-- вывод всех читателей, которые когда либо просрочили сдачу книг
-- select s.id, s.last_name, s.first_name, s.patronymic, count(*) from issues i
-- join students s on i.students_id = s.id
-- where (i.return_date - i.issue_date) > 14
--     group by s.id
--     order by count(*) desc

-- поиск самого злостного читателя
select s.id, s.last_name, s.first_name, s.patronymic, count(*) from issues i
join students s on i.students_id = s.id
where (i.return_date - i.issue_date) > 14
    group by s.id
    order by count(*) desc
    LIMIT 1;


