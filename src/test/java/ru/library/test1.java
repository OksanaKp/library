package ru.library;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.library.models.Students;
import ru.library.service.IssuesService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class test1 {

    @Autowired
    IssuesService issuesService;

    @Test
    public void findReader(){
        Students worstReader = issuesService.findWorstReader(14);
        System.out.println("Самый злостный читатель:");
        System.out.println(worstReader.getLastName() + " " + worstReader.getFirstName() + " " +
                worstReader.getPatronymic() );
    }

}
